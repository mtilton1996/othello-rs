#![feature(test)]
#![feature(asm)]
/*
Matthew Tilton
102-35-068
11-11-18
Assignment 3
This program implements a game of othello along with a few types of ai to play the game.
*/
extern crate test;

mod board;
mod enums;
mod tui;
mod AI;
mod tree;
mod heuristics;

fn main() {
    tui::tui();
}